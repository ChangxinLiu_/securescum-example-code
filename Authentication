//Weak Passwords and Password Functionality
//Simply checking that a password is not NULL is not sufficient:

String password = request.getParameter("Password");
if (password == Null) 
   {throw InvalidPasswordException()
   }

//Passwords should be checked for the following composition or a variance of such
//at least: 1 uppercase character (A-Z)
//at least: 1 lowercase character (a-z)
//at least: 1 digit (0-9)
//at least one special character (!"£$%&...)
//a defined minimum length (8 chars)
//a defined maximum length (as with all external input)
//no contiguous characters (123abcd)
//not more than 2 identical characters in a row (1111)
//Such rules should be looked for in code and used as soon as the http request is received. The rules can be complex RegEx expressions or logical code statements:

if password.RegEx([a-z])
   and password.RegEx([A-Z])
   and password.RegEx([0-9])
   and password.RegEx({8-30})
   and password.RexEX([!"£$%^&*()])
   return true;
else
return false;





//Password Storage Strategy

import java.security.MessageDigest;
public byte[] getHash(String password) throws NoSuchAlgorithmException {
      MessageDigest digest = MessageDigest.getInstance("SHA-1");
      digest.reset();
      byte[] input = digest.digest(password.getBytes("UTF-8"));
      
      
//Salting: Storing simply hashed passwords has its issues, such as the possibility to identify two identical passwords (identical hashes) and also the birthday attack. 
//A countermeasure for such issues is to introduce a salt. A salt is a random number of a fixed length. This salt must be different for each stored entry. It must be stored as clear text next to the hashed password:


import java.security.MessageDigest;
public byte[] getHash(String password, byte[] salt) throws NoSuchAlgorithmException {
      MessageDigest digest = MessageDigest.getInstance("SHA-256");
      digest.reset();
      digest.update(salt);
      return digest.digest(password.getBytes("UTF-8"));
}







//1. Protecting user credentials
//password gets stored in database in plain text . Here is related code in data/user-dao.js addUser()method:

// Create user document
var user = {
    userName: userName,
    firstName: firstName,
    lastName: lastName,
    password: password //received from request param
};
                        
//To secure it, handle password storage in a safer way by using one way encryption using salt hashing as below:
// Generate password hash

var salt = bcrypt.genSaltSync();
var passwordHash = bcrypt.hashSync(password, salt);

// Create user document
var user = {
    userName: userName,
    firstName: firstName,
    lastName: lastName,
    password: passwordHash
};
                        
//This hash password can not be decrypted, hence more secure. To compare the password when user logs in, the user entered password gets converted to hash and compared with the hash in storage.
if (bcrypt.compareSync(password, user.password)) {
    callback(null, user);
} else {
    callback(invalidPasswordError, null);
}
                        
//Note: The bcrypt module also provides asynchronous methods for creating and comparing hash.



//2. Session timeout and protecting cookies in transit
//The insecure demo application does not contain any provision to timeout user session. The session stays active until user explicitly logs out.

//In addition to that, the app does not prevent cookies being accessed in script, making application vulnerable to Cross Site Scripting (XSS) attacks. Also cookies are not prevented to get sent on insecure HTTP connection.

//To secure the application:

//    1. Use session based timeouts, terminate session when browser closes.

// Enable session management using express middleware
app.use(express.cookieParser());
 
//    2. In addition, sets HTTPOnlyHTTP header preventing cookies being accessed by scripts. The application used HTTPS secure connections, and cookies are configured to be sent only on Secure HTTPS connections by setting Secureflag.

app.use(express.session({
    secret: "s3Cur3",
    cookie: {
        httpOnly: true,
        secure: true
    }
}));
                        
//    3. When user clicks logout, destroy the session and session cookie

req.session.destroy(function() {
    res.redirect("/");
});
                        
//Note: The example code uses MemoryStoreto manage session data, which is not designed for production environment, as it will leak memory, and will not scale past a single process. 
//Use database based storage MongoStore or RedisStore for production. Alternatively, sessions can be managed using popular passport module.




//3. Session hijacking
//The insecure demo application does not regenerate a new session id upon user's login, therefore rendering a vulnerability of session hijacking if an attacker is able to somehow steal the cookie with the session id and use it.

//Upon login, a security best practice with regards to cookies session management would be to regenerate the session id so that if an id was already created for a user on an insecure medium (i.e: non-HTTPS website or otherwise), 
//or if an attacker was able to get their hands on the cookie id before the user logged-in, then the old session id will render useless as the logged-in user with new privileges holds a new session id now.

//To secure the application:

//1. Re-generate a new session id upon login (and best practice is to keep regenerating them upon requests or at least upon sensitive actions like a user's password reset. 
//Re-generate a session id as follows: By wrapping the below code as a function callback for the method req.session.regenerate()

req.session.regenerate(function() {

  req.session.userId = user._id;

  if (user.isAdmin) {
    return res.redirect("/benefits");
  } else {
    return res.redirect("/dashboard");
  }

})